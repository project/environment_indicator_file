Display an arbitrary tag from a file in your sites folder.

Useful for when using different build systems across multiple environments and want to standardise.

File must be called "version" and live in sites/$site_name
